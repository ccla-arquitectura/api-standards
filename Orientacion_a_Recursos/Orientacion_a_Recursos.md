## Orientación a Recursos

El concepto fundamental en cualquier API RESTful es el **_recurso_**. Un recurso es un objeto o entidad de negocio con un tipo, estructura de datos asociados, relaciones con otros recursos y un conjunto de métodos que operan en él. Es similar a una instancia de objeto en un lenguaje de programación orientado a objetos, con la importante diferencia de que solo se definen unos pocos métodos estándar para el recurso (correspondiente a los métodos HTTP: `GET`, `POST`, `PUT` y `DELETE` estándar), mientras que una instancia de objeto normalmente tiene muchos métodos.

![Rest Resource Model](resources/restresourcemodel.png)