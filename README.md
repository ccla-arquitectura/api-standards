# Guía de Diseño API REST

## Introduccion

Esta guía agrupa los lineamientos, pautas y buenas practicas a cumplir en el diseño de Interfaces de Programación de Aplicaciones (del ingles, _Application Programming Interfaces_ o API) para Caja Los Andes. La misma pretende ser la fuente autorizada para este punto en nuestra organización. 

El enfoque inicial está en REST (_Representational State Transfer_), y se modeló a partir de guías de estilo externas como PayPal, Haufe, Twilio, Capital One y Google Apigee. Estilos adicionales pueden ser agregado en el futuro.

El término diseño de API hace referencia a la fase, dentro del ciclo de desarrollo, en la cual se conceptualiza la misma dentro de su alcanse como interfaz en la comunicación System-to-System. En otras palabras, considerandola como el contrato entre un proveedor de información (sistema servidor) y un consumidor (sistema cliente). Este establece el contenido que debe entregar el consumidor (_request_) y el que debe entegar el productor (_response_).

Al diseñar las APIs, es importante ser coherente y apoyar el desarrollo independiente en toda la organización. El darse cuenta de este nivel de la coherencia requerirá que se cumpla con los requisitos mínimos de diseño API descritos en esta guía.

Los diferentes artefactos, que hacen en su conjunto a esta guía de diseño, no están en un estado totalmente completo. Los mismos deben considerarse como en trabajo en progreso (_work in progress_), es decir, documentos vivos incrementales.

&nbsp;

## Alcance

Los documentos agrupados en esta guía están dirigidos a personal del ambito de Tecnologías de la Información (del ingles, _Information Technology_ o IT) involucrado en el análisis, diseño, implementación y administración de sistemas IT.

El alcance de los siguientes artefactos es especificar los lineamientos, premisas y buenas practicas a cumplir en la construcción de un API durante su fase de Diseño. Esto dentro de su alcanse como interfaz de comunicación System-to-System.

Los artefatos no buscan plantear la definición o aclaración de conceptos y estándares de mercado; tampoco aspectos referentes a la metodología para la construcción de las mismas.

Los lineamientos, premisas y buenas practicas planteados son agnósticos al stack de herramientas utilizados para la implementación.

&nbsp;

## Índice

[Guía de Diseño API REST](#guía-de-diseño-api-rest)
  - [Introduccion](#introduccion)
  - [Alcance](#alcance)
  - [Índice](#índice)
  - [Orientación a Recursos](Orientacion_a_Recursos/Orientacion_a_Recursos.md)
  - [Estrategia de Versionamiento](Estrategia_de_Versionamiento/Estrategia_de_Versionamiento.md)
  - [Componentes de URI](Componentes_de_URI/Componentes_de_URI.md)
  - [Verbos HTTP](Verbos_HTTP/Verbos_HTTP.md)
  - Tópicos de diseño (PROXIMAMENTE...)
    - Leer una colección de recursos
    - Leer un recurso único
    - Crear un recurso en la colección
    - Actualizar un recurso único
    - Eliminar un recurso único
    - Sub-recursos
    - Colección de sub-recursos
    - Sub-Recurso Singleton
    - Filtración
    - Paginación
    - Clasificación
    - Búsqueda
    - Búsqueda - Paginación

&nbsp;
