## Versionamiento

TODO: Complementar estas dos secciones con lo explicado en: 
- "API Design and Fundamentals of Google Cloud's Apigee API Platform 01 - Transcripcion.md", seccion "5.5. API Versioning Strategy"
- Workshop de ApiService: "01 - Buenas prácticas en el diseño y desarrollo de APIs + Estrategias de Modelamiento de Productos.gslides"


### Los servicios REST deben ser compatibles con versiones anteriores

Los *endpoints* REST son dependencias remotas en tiempo de ejecución (los compiladores no pueden detectarlos en tiempo de compilación) y cualquier cambio podría interrumpir de inmediato el consumo de ellos. Se debe tener en cuenta que hacer un análisis de impacto en los consumidores API REST es diferente al análisis de impacto tradicional contra un código fuente Sistemas Centralizados. Los consumidores pueden ser aplicaciones dentro de Sistemas Distribuidos, clientes de escritorio o cualquier sistema cliente que pueda comunicarse mediante el protocolo HTTP. Si bien las herramientas como un API gateway tienen algunas facilidades para comprender a los consumidores de las API REST, la regla es que no se deben realizar cambios importantes en las APIs. Lo que esto significa es que si (después de pensarlo mucho) elige versionar un API, debe admitir versiones concurrentes de la misma que se expondrán durante un período de tiempo a la vez. Cómo se logra esto será diferente entre las implementaciones de nivel 2 y nivel 3 del Modelo de Madurez de Richardson (_Richardson Maturity Model_ o RMM). 

Los siguientes tipos de cambios pueden afectar a los consumidores de una API REST:

  - Cambiar atributos en endpoints existentes.
  - Eliminar endpoints y/o atributos en endpoints existentes.
  - Cambio de modelos de seguridad.

&nbsp;

### Desarrolle API de forma segura

Cuando se necesitan cambios, los desarrolladores deben favorecer hacer cambios sin interrupciones en las APIs existentes, en lugar de introducir una nueva versión. 

Los siguientes tipos de cambios generalmente no afectarán a los consumidores existentes de API REST:

  - Agregar nuevos atributos.
  - Agregar nuevos endpoints.

&nbsp;


