## Estrategia de Versionamiento

### Nomenclatura

Los URI deben incluir un número de versión principal en forma de `/vN` donde `N` es el número de versión principal. La versión debe indicarse en el segundo nodo de un URI justo después del nodo `/{nombre-del-API}`. Si bien en contra de la orientación de la industria de preferir los especificadores de versión más temprano en la ruta, las unidades de negocios pueden tomar la decisión de versionar a nivel de recursos más adelante en la ruta para proporcionar un vehículo para versiones muy granulares.

URI Template
```
/{nombre-del-API}/v{version}/
```

Ejemplo 
```
/personas/v1
```

El URI debe incluir `/vN` con la versión principal reemplazando `N`.

El control de versiones basado en URI se utiliza por su simplicidad de uso para consumidores de API frente a otros métodos más complejos, como un enfoque basado en header.

> **Premisa:**
> 
> Si bien es posible tener varias versiones, se desaconseja encarecidamente.
> 
> Si bien es estándar indicar un número de versión para la coherencia de la API, tener que admitir múltiples versiones agrega sobrecarga y complejidad. Por lo tanto, los desarrolladores deberían intentar raramente crear nuevas versiones. Las APIs REST no se consideran bibliotecas compuestas más pequeñas y granulares como los JARs/etc. donde tener muchas versiones (es decir, versiones semánticas usando <major.minor.patch>) es común.

&nbsp;
