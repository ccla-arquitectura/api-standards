## Verbos HTTP

La siguiente tabla ofrece una descripción general de los verbos HTTP más utilizados en la creación de servicios web para realizar operaciones tanto en colecciones como en recursos únicos.

| Verbo | Uso | Idempotente | Colección (`/<recurso>`) | Recurso (`/<recurso>/<recurso-id>`)
| --- | --- |:---:| --- | --- |
| `GET`| Para **leer**. | X | Lee o retorna una colección de recursos|Lee o retorna un recurso único|
| `POST`| Para **crear** un recurso, o **ejecutar** una operación compleja dentro de un recurso. |   | Crea un nuevo recurso | Invalido, retorna error con HTTP status code: `400 - Bad Request` |
| `PUT`| Para **actualizar**. | X | Actualiza una colección de recursos | Actualiza recurso único, si no existe retorna un error con HTTP status code: `400 - Bad Request` |
| `DELETE`| Para **eliminar**. | X | Elimina una colección de recursos | Eliminar recurso único, si no existe retorna un error con HTTP status code: `400 - Bad Request` |

&nbsp;

> **Idempotente** significa que se puede hacer la misma llamada repetidamente produciendo el mismo resultado en el servidor. En otras palabras, hacer múltiples solicitudes idénticas tiene el mismo efecto que hacer una sola solicitud. Tenga en cuenta que, si bien las operaciones idempotentes producen el mismo resultado en el servidor (sin efectos secundarios), la respuesta en sí misma puede no ser la misma (por ejemplo, el estado de un recurso puede cambiar entre solicitudes o el recurso puede no existir y generar un error).

&nbsp;
