## Componentes de URI

Los componentes principales de un Identificador de Recursos Uniforme (del inglés, _Uniform Resource Identifier_ o URI) para un API RESTfull deben alinearse al siguiente template: 

**Template de URI**:
```
<scheme>://<authority>/<nombre-del-API>/<version>/<recurso>[/<id-del-recurso>[/<sub-recurso>[/<id-del-sub-recurso>]]][?<query>]
```

En las secciones siguientes se dará mayor aclaración de cada uno de los nodos de este template de URI anterior.

&nbsp;

### Nomenclatura de URI

Toda URI, de un API RESTful, debe cumplir las siguientes pautas en su nomenclatura:

- Los URI deben estar en minúsculas. 
- Cuando existen varios términos para un nodo dado dentro de una URI, deben estar separados por un "-" (guión). 
- No debe utilizarse *camel case*, la separación de subrayado, etc.
- Los parámetros de la cadena de consulta (_query params_) deben estar en *camel case*.

&nbsp;

### Scheme (Esquema)

El primer nodo del URI identifica el protocolo de acceso al recurso.

Se **debe usar HTTPS**. HTTP **no debe** ser utilizado.

&nbsp;

### Authority (Autoridad)

El nodo _authority_ esta dado por el nombre de _host_ que identifica la plataforma que actua como sistema servidor y que expone las URIs de una API o varias APIs.

Los sistemas clientes, consumidores de APIs, deben hacer **referencia al host utilizando siempre su _host name_ (nombre de dominio de red),  o _alias_**. **No debe utilizarse el numero de IP**.

Ejemplos:
```
https://api.example.cl
https://api-dev.example.cl
```

&nbsp;

### Nombre del API

Dentro del _path_ (ruta) del URI, el primer nodo representa un espacio de nombre empresarial único para el API expuesta. Este **debe identificar el Dominio Empresarial** para el cual el API en cuestión expone sus recursos, y funcionalidades sobre los mismos.

Ejemplos:
```
https://api.example.com/personas
https://api.example.com/empresas
https://api.example.com/creditos-hipotecarios
```

Para mayor referencia sobre los Dominios Empresariales, ver el siguiente [link](https://sites.google.com/cajalosandes.cl/arquitectura/fundamentos-transversales/dominios-empresariales).

&nbsp;

### Ruta Base (_Base Path_)

La _ruta base_ (_base path_) es la primera parte del URI que sigue al nodo de authority. Así el base path esta conformado por los nodos _nombre del API_ y _version_ (para este ultimo nodo se recuerda ver la sección [Estrategia de Versionamiento](../Estrategia_de_Versionamiento/Estrategia_de_Versionamiento.md)).

De esta manera en la siguiente URI de ejemplo:
```
https://api.example.com/personas/v2/...
```
se puede visualizar que `/personas/v2` es el base path del URI de la API de "Personas", en su versión 2, de "example.com".

> IMPORTANTE:
>
> Para implementar y exponer un API, cada combinación de authority y base path debe 
ser única.

&nbsp;

### Referencias de recursos en el path del URI

La abstracción clave de la información en APIs RESTful es el _recurso_, en otras palabras, la _entidad de negocio_. Las APIs RESTful deben centrarse en los recursos sobre los que se actúa, en lugar de centrarse en una lista de operaciones o acciones. **Todo en REST debe modelarse como un recurso**.

Los recursos deben especificarse en las URIs de la API, luego del base path, mediante el uso de nombres (sustantivos) e IDs de recursos. El verbo o método HTTP de el request especifica el tipo de operación que se está realizando en el recurso o lista de recursos. Este punto es ampliado en la sección [Verbos HTTP](.../Verbos_HTTP/Verbos_HTTP.md).

Por ejemplo, para acceder a una lista de "ofertas de trabajo", debe realizar una solicitud GET a `/ofertas-de-trabajo`. Para acceder a la "oferta de trabajo" cuyo ID es "1234", debe realizar una solicitud GET a `/ofertas-de-trabajo/1234`.

> ACLARACIÓN:
>
> Frecuentemente, cuando se especifican llamadas **en el contexto de una API** previamente conocida, se hace referencia eliminando el nodo autority y base path del URI. En el primer ejemplo anterior el URI completo podría ser: GET `https://api.example.com/recursos-humanos/v1/ofertas-de-trabajo`.

El path del URI es uno de los puntos de diseño más importante de una API. La utilización de los conceptos anteriores, tienen el objetivo de que la misma sea simple e intuitiva y pueda ser entendida por cualquiera que entienda REST.

&nbsp;

#### Premisas en la referencia de recursos en un URI

Además las pautas listadas en la sección [Nomenclatura de URI](#Nomenclatura-de-URI), las referencias a recursos deben cumplir las siguientes premisas y pautas en su nomenclatura:

  - Los recursos deben ser nombrados **utilizando sustantivos**.  
    Evitar el uso de verbos.
  - Mantener los sustantivos **en plural**.
    - `/empleados/24252762`, NO `/empleado/24252762`
  - Priorizar el uso de **nombres concretos**, sobre la utilización de abstracciones.   
    Cuanto mas contreto sea el nombre de un recurso, menor será la ambigüedad de interpretación. Esto facilita el entendimiento correcto del API por parte de los desarrolladores de las aplicaciones o sistemas clientes.
    - `/solicitudes-de-egreso-monetario/AR4897X`, NO `/solicitudes/AR4897X`
  - Priorizar el uso de **términos estándar** sobre la terminología interna de la empresa.  
    Se recuerda que también se está diseñando los recursos para que los desarrolladores de aplicaciones lo entiendan, quienes pueden no tener conocimiento de los nombres y conceptos internos utilizados dentro de la empresa.
    - `/empleados`, NO `/colaboradores-CLA`
  - Mantener, en lo posible, la referencia a un recurso principal y sus relaciones con otros recursos en dos niveles.
    - `/ofertas-de-trabajo/1234/vacantes/3`

&nbsp;
